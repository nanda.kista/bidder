package com.example.bidder.ui.activity.notifikasi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bidder.R
import com.example.bidder.adapter.NotificationAdapter
import com.example.bidder.common.base.BaseActivity
import com.example.bidder.common.base.BaseContract
import com.example.bidder.dummy.NotifData
import com.example.bidder.model.Barang
import com.example.bidder.model.Notif
import kotlinx.android.synthetic.main.activity_notifikasi.*

class NotifikasiActivity : BaseActivity(), NotifikasiContract.View {

    private var presenter =  NotifikasiPresenter()
    private var listNotif: ArrayList<Notif> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifikasi)
        presenter.attach(this)

        //TOOLBAR
        setSupportActionBar(tb_notif)
        supportActionBar?.title = "Notifikasi"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        showRecyclerList(listNotif)
        listNotif.addAll(NotifData.listBarang)
    }

    override fun onInit() {
        initView()
    }

    override fun initView() {
        rv_notif.setHasFixedSize(true)
        onSwipeRefresh()
    }

    override fun showRecyclerList(list: ArrayList<Notif>) {
        val listRiwayat = NotificationAdapter(list)
        rv_notif.layoutManager = LinearLayoutManager(this)
        rv_notif.adapter = listRiwayat
        presenter.onLoading(false)
    }

    override fun progressBarShow() {
        pb_notif.visibility = View.GONE
    }

    override fun progressBarHide() {
        pb_notif.visibility = View.GONE
    }

    override fun onErrorGetNotif(msg: String) {
        Log.d("Notifikasi Activity : ", msg)
        Toast.makeText(this, "Cek koneksi internet anda", Toast.LENGTH_SHORT).show()
    }

    override fun onSwipeRefresh() {
        sr_notif.setOnRefreshListener {
            presenter.onLoading(true)
            presenter.getListNotif()
            Handler().postDelayed({ sr_notif.isRefreshing = false }, 500)
        }
    }
}
