package com.example.bidder.ui.activity.login
import android.text.Editable
import com.example.bidder.common.base.BaseContract
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


class LoginContract {

    interface View : BaseContract.View {
        fun initView()
        fun showErrorWrong()
        fun showErrorEmpty()
        fun onSuccess()
        fun onClickListener()

        fun showProgressDialog()
        fun dismissProgressDialog()

    }

    interface Presenter : BaseContract.Presenter<View> {
        fun checkValidation(username: String, password: String)
        fun afterTextChanged(s: Editable, etLayout: TextInputLayout, etInput: TextInputEditText)
    }
}