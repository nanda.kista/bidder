package com.example.bidder.ui.activity.login

import android.text.Editable
import android.util.Log
import com.example.bidder.common.Fungsiku
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.example.bidder.common.helper.ValidationHelper
import io.reactivex.disposables.CompositeDisposable

class LoginPresenter : LoginContract.Presenter {
    private val TAG = "LoginPresenter"

    private lateinit var view: LoginContract.View
    private val subscriptions = CompositeDisposable()

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: LoginContract.View) {
        this.view = view
    }

    override fun checkValidation(username: String, password: String) {
        //Field Validation
        if(username.isEmpty() || password.isEmpty()) {
            view.dismissProgressDialog()
            view.showErrorEmpty()
        }
        else if(username == "admin" && password == "bidder") {
            view.dismissProgressDialog()
            view.onSuccess()

        } else {
            Log.d(TAG, "cek username: $username, password: $password")
            view.dismissProgressDialog()
            view.showErrorWrong()
        }
    }

    override fun afterTextChanged(
        s: Editable,
        etLayout: TextInputLayout,
        etInput: TextInputEditText
    ) {
        if (s.isEmpty()) {
            etLayout.error = "Harap diisi terlebih dahulu!"
        } else
            if (!ValidationHelper.layoutChangeOnValidation(etInput, etLayout)) {
                etLayout.error = null
            }
    }

}