package com.example.bidder.ui.activity.detail_barang

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bidder.R
import kotlinx.android.synthetic.main.activity_detail_notif__dummy.*

class DetailNotif_DummyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_notif__dummy)

        //TOOLBAR
        setSupportActionBar(tb_detail_notif)
        supportActionBar?.title = "Detail Barang"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
