package com.example.bidder.ui.activity.register

import android.text.Editable
import android.util.Log
import com.example.bidder.common.Fungsiku
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.example.bidder.common.helper.ValidationHelper
import io.reactivex.disposables.CompositeDisposable

class RegisterPresenter : RegisterContract.Presenter {
    private val TAG = "RegisterPresenter"

    private lateinit var view: RegisterContract.View
    private val subscriptions = CompositeDisposable()

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: RegisterContract.View) {
        this.view = view
    }

    override fun checkValidation(username: String, password: String, noTelp : String, repassword : String) {

        //Field Validation
        if(username.isEmpty() || password.isEmpty() || noTelp.isEmpty() || repassword.isEmpty()) {
            view.dismissProgressDialog()
            view.showErrorEmpty()
        } else if(!ValidationHelper.isValidPassword(password)) {
            view.validPassword()
            view.dismissProgressDialog()
        } else if (password != repassword) {
            view.confirmPassword()
            view.dismissProgressDialog()
        } else if (!ValidationHelper.isValidPhoneNumber(noTelp)) {
            view.validPhoneNumber()
            view.dismissProgressDialog()
        }
        else {
            view.dismissProgressDialog()
            view.onSuccess()
        }
    }
}