package com.example.bidder.ui.fragment.aktivitas

import com.example.bidder.common.base.BaseContract
import com.example.bidder.model.Barang

class AktivitasFragmentContract {
    interface View : BaseContract.View {
        fun initView()

        fun showRecyclerList(list : ArrayList<Barang>)
        fun progressBarShow()
        fun progressBarHide()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun onLoading(state:Boolean)
        fun getListBarang()
    }
}