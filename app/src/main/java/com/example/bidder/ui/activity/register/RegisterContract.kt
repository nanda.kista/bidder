package com.example.bidder.ui.activity.register
import com.example.bidder.common.base.BaseContract

class RegisterContract {

    interface View : BaseContract.View {
        fun initView()
        fun showErrorEmpty()
        fun onSuccess()
        fun confirmPassword()
        fun showProgressDialog()
        fun dismissProgressDialog()
        fun validPassword()
        fun validPhoneNumber()
        fun showPopUp()

    }

    interface Presenter : BaseContract.Presenter<View> {
        fun checkValidation(username: String, password: String, noTelp : String, repassword : String)
    }
}