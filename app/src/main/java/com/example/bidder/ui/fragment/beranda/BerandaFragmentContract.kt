package com.example.bidder.ui.fragment.aktivitas

import com.example.bidder.common.base.BaseContract

class BerandaFragmentContract {
    interface View : BaseContract.View {
        fun initView()
        fun onClickListener()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun postData()
    }
}