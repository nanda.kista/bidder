package com.example.bidder.ui.activity.register

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import com.example.bidder.R
import com.example.bidder.common.Fungsiku
import com.example.bidder.common.base.BaseActivity
import com.example.bidder.common.helper.ValidationHelper
import com.example.bidder.sharedpreference.KeySharedPreference
import com.example.bidder.sharedpreference.SharedPreference
import com.example.bidder.sharedpreference.TokenDummy
import com.example.bidder.ui.activity.dashboard.DashboardActivity
import com.example.bidder.ui.activity.login.LoginActivity
import kotlinx.android.synthetic.main.activity_register_activity.*
import kotlinx.android.synthetic.main.activity_register_activity.btn_daftar
import kotlinx.android.synthetic.main.item_popup_success.*

class RegisterActivity : BaseActivity(), RegisterContract.View {

    val TAG = "RegisterActivity"
    val presenter = RegisterPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_activity)
        presenter.attach(this)

        //Toolbar
        setSupportActionBar(tb_daftar)
        supportActionBar?.title = "Registrasi"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onInit() {
        initView()
    }

    override fun initView() {

        val errorMessage = "Harap diisi terlebih dahulu!"
        ValidationHelper.isValidMaterialEditText(et_regis_email_layout, et_regis_username, errorMessage)
        ValidationHelper.isValidMaterialEditText(et_regis_password_layout, et_regis_password, errorMessage)
        ValidationHelper.isValidMaterialEditText(et_regis_telp_layout, et_regis_telp, errorMessage)
        ValidationHelper.isValidMaterialEditText(et_regis_konfirmasi_password_layout, et_regis_konfirmasi_password, errorMessage)

        btn_daftar.setOnClickListener {
            val username = et_regis_username.text.toString()
            val password  = et_regis_password.text.toString()
            val konfirm = et_regis_konfirmasi_password.text.toString()
            val noTelp = et_regis_telp.text.toString()
            showProgressDialog()
            presenter.checkValidation(username, password, noTelp, konfirm)
        }

        btn_go_login.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    override fun showErrorEmpty() {
        Fungsiku.showPopUpDialogWaring(this, "Registrasi gagal !", "Semua field harus diisi.")
    }

    override fun onSuccess() {
        val sharedPref = SharedPreference(applicationContext)
        sharedPref.save(KeySharedPreference.KEY_TOKEN, TokenDummy.TOKEN)
        Log.d(TAG, "$sharedPref")
        showPopUp()
    }

    override fun validPassword() {
        et_regis_password_layout.error = "Password minimal terdiri dari 5 karakter"
    }

    override fun validPhoneNumber() {
        et_regis_telp_layout.error = "No. Telephone tidak valid!"
    }

    override fun showPopUp() {
        val epicDialog = Dialog(this)
        epicDialog.setContentView(R.layout.item_popup_success)
        epicDialog.tv_popup_title.text = "Pendaftaran Berhasil"
        epicDialog.tv_popup_message.text = "Selamat pendaftaran berhasil dilakukan !"
        epicDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        epicDialog.show()
        epicDialog.setCanceledOnTouchOutside(false)

        epicDialog.btn_ok_regis.setOnClickListener {
            epicDialog.dismiss()
            val intent = Intent(this, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }


    override fun confirmPassword() {
        et_regis_konfirmasi_password_layout.error = "Password tidak cocok"
    }

    override fun showProgressDialog() {
        Fungsiku.showProgressDialog(this, "Loading...")
    }

    override fun dismissProgressDialog() {
        Fungsiku.dismissProgressDialog()
    }
}
