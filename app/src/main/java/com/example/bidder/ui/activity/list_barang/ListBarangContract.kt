package com.example.bidder.ui.activity.list_barang

import com.example.bidder.model.Barang
import com.example.bidder.common.base.BaseContract

class ListBarangContract {
    interface View : BaseContract.View {

        fun openDetail()
        fun initView()
        fun showRecyclerList(list : ArrayList<Barang>)
        fun progressBarShow()
        fun progressBarHide()
        fun onErrorGetListKaryawan(msg : String)
        fun onSwipeRefresh()
    }

    //@PreActivity
    interface Presenter : BaseContract.Presenter<View> {
        fun onLoading(state:Boolean)
        fun getListBarang()
    }
}