package com.example.bidder.ui.activity.list_barang

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.bidder.R
import com.example.bidder.adapter.ListBarangAdapter
import com.example.bidder.common.base.BaseActivity
import com.example.bidder.dummy.BarangData
import com.example.bidder.model.Barang
import kotlinx.android.synthetic.main.activity_list_barang.*

class ListBarangActivity : BaseActivity(), ListBarangContract.View {

    private var presenter =  ListBarangPresenter()
    private var listBarang: ArrayList<Barang> = arrayListOf()

   // private val movie: java.util.ArrayList<ModelFilm> = java.util.ArrayList<ModelFilm>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_barang)
        presenter.attach(this)

        //TOOLBAR
        setSupportActionBar(tb_list_lelang_barang)
        supportActionBar?.title = "Lelang Nama Barang"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        showRecyclerList(listBarang)
        listBarang.addAll(BarangData.listBarang)
    }

    override fun onInit() {
        initView()
    }

    override fun openDetail() {

    }

    override fun initView() {
        rv_list_lelang_barang.setHasFixedSize(true)
        onSwipeRefresh()
    }

    override fun showRecyclerList(list: ArrayList<Barang>) {
        val listLelangSmartphone = ListBarangAdapter(list)
        rv_list_lelang_barang.layoutManager = GridLayoutManager(this, 2)
        rv_list_lelang_barang.adapter = listLelangSmartphone
        presenter.onLoading(false)
    }

    override fun progressBarShow() {
        pb_list_lelang_barang.visibility = View.VISIBLE
    }

    override fun progressBarHide() {
        pb_list_lelang_barang.visibility = View.GONE
    }

    override fun onErrorGetListKaryawan(msg: String) {
        Log.d("List Barang Activity : ", msg)
        Toast.makeText(this, "Cek koneksi internet anda", Toast.LENGTH_SHORT).show()
    }

    override fun onSwipeRefresh() {
        sr_list_lelang_barang.setOnRefreshListener {
        presenter.onLoading(true)
        presenter.getListBarang()
        Handler().postDelayed({ sr_list_lelang_barang.isRefreshing = false }, 500)
        }
    }
}
