package com.example.bidder.ui.fragment.aktivitas

import io.reactivex.disposables.CompositeDisposable

class BerandaFragmentPresenter : BerandaFragmentContract.Presenter {


    private val TAG = "ListPresenter"

    private lateinit var view: BerandaFragmentContract.View
    private val subscriptions = CompositeDisposable()

    override fun postData() {
        //DO SOMETHING
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: BerandaFragmentContract.View) {
        this.view = view
    }

}