package com.example.bidder.ui.activity.detail_barang

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.models.SlideModel
import com.example.bidder.R
import com.example.bidder.common.Fungsiku
import com.example.bidder.common.base.BaseActivity
import com.example.bidder.sharedpreference.KeySharedPreference
import com.example.bidder.sharedpreference.SharedPreference
import com.example.bidder.ui.dialog.BidDialog
import kotlinx.android.synthetic.main.activity_detail_barang.*


class DetailBarangActivity : BaseActivity(), DetailBarangContract.View, BidDialog.BidDialogListener {

    private var presenter = DetailBarangPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_barang)
        presenter.attach(this)

        //TOOLBAR
        setSupportActionBar(tb_detail_lelang)
        supportActionBar?.title = "Detail Barang"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter.getDescription()

    }

    override fun onInit() {
        initView()
    }

    override fun initView() {
        onSwipeRefresh()
        onClickListener()
    }

    private fun openDialog() {
        val bidDialog = BidDialog()
        bidDialog.show(supportFragmentManager, "bid dialog")
    }

    override fun progressBarHide() {
        pb_detail_lelang.visibility = View.GONE
        pb_photo_detail.visibility= GONE
    }

    override fun progressBarShow() {
        pb_detail_lelang.visibility = View.VISIBLE
        pb_photo_detail.visibility= VISIBLE
    }

    override fun showDetail() {
        val photo = intent.getStringExtra(EXTRA_PHOTO)
        val bid = intent.getStringExtra(EXTRA_BID)
        val status = intent.getIntExtra(EXTRA_STATUS, 0)

        Log.d("Detail Activity ", status.toString())

        tv_detail_judul_barang.text = intent.getStringExtra(EXTRA_NAMA)
        tv_detail_harga_barang.text = intent.getStringExtra(EXTRA_HARGA)
        tv_detail_waktu.text = intent.getStringExtra(EXTRA_WAKTU)
        tv_detail_tgl.text = intent.getStringExtra(EXTRA_TANGGAL)
        tv_detail_lokasi.text = intent.getStringExtra(EXTRA_LOKASI)
        tv_detail_uang_jaminan.text = intent.getStringExtra(EXTRA_JAMINAN)
        tv_detail_penyelenggara.text = intent.getStringExtra(EXTRA_PENYELENGGARA)
        tv_detail_bid_teratas.text = "Rp $bid"

        when (status) {
            2 -> {
                tv_detail_status.visibility = GONE
            }
        }

        val imageList = ArrayList<SlideModel>()
        imageList.add(SlideModel(photo))
        imageList.add(SlideModel("https://m.jpnn.com/timthumb.php?src=https://photo.jpnn.com/arsip/watermark/2019/12/02/xiaomi-redmi-foto-gizchina-79.jpg&w=600&h=320&a=t&zc=0&q=80",
            true))
//        imageList.add(SlideModel("https://2.bp.blogspot.com/-CyLH9NnPoAo/XJUWK2UHiMI/AAAAAAAABUk/D8XMUIGhDbwEhC29dQb-7gfYb16GysaQgCLcBGAs/s1600/tiger.jpg"))
//        imageList.add(SlideModel("https://3.bp.blogspot.com/-uJtCbNrBzEc/XJUWQPOSrfI/AAAAAAAABUs/ZlReSwpfI3Ack60629Rv0N8hSrPFHb3TACLcBGAs/s1600/elephant.jpg", "The population of elephants is decreasing in the world."))
        val imageSlider = findViewById<ImageSlider>(R.id.iv_detail_photo)
        imageSlider.setImageList(imageList)
    }

    override fun onClickListener() {
        btn_ikuti_lelang.setOnClickListener {
            btn_ajukan_bid.setBackgroundResource(R.color.colorPrimary)
            btn_ajukan_bid.visibility = View.VISIBLE
            btn_ikuti_lelang.visibility = View.GONE
        }

        btn_ajukan_bid.setOnClickListener {
            openDialog()
        }
    }

    override fun onSwipeRefresh() { sr_detail_lelang.setOnClickListener {
            presenter.onLoading(true)
            presenter.getDescription()
            Handler().postDelayed({ sr_detail_lelang.isRefreshing = false }, 500)
        }
    }

    companion object {
        val EXTRA_NAMA = "nama_barang"
        val EXTRA_STATUS = "nama_status"
        val EXTRA_PHOTO = "photo_barang"
        val EXTRA_HARGA = "harga_barang"
        val EXTRA_DESKRIPSI = "deskripsi_barang"
        val EXTRA_TANGGAL = "tgl_barang"
        val EXTRA_WAKTU = "waktu_barang"
        val EXTRA_LOKASI = "lokasi_barang"
        val EXTRA_PENYELENGGARA = "penyelenggara_barang"
        val EXTRA_BID = "bid_teratas_barang"
        val EXTRA_JAMINAN = "uang_jaminan_barang"
    }

    @SuppressLint("SetTextI18n")
    override fun applyTexts(nilaiBid: String?) {
        val sharedPref = SharedPreference(applicationContext)

        val bidInputInt = nilaiBid?.toLong()
        Log.d("BID INPUT :", bidInputInt.toString())
        val bid = intent.getStringExtra(EXTRA_BID)
        val bidTeratas = bid.toLong()

        //LOGIKA IF Untuk menentukan update nilai Bid Teratas
        if (bidInputInt != null) {
            if (bidInputInt <= bidTeratas) {
                tv_detail_bid_teratas.text = "Rp $bidTeratas"
                Fungsiku.showPopUpDialogWaring(this, "Gagal mengajukan Bid", "Tawaran harus lebih besar dari bid teratas" )
            } else {
                tv_detail_bid_teratas.text = "Rp $nilaiBid"
                Fungsiku.showPopUpDialog(this, "Sukses", "Berhasil mengajukan Bid" )

            }
        }



    }
}
