package com.example.bidder.ui.fragment.beranda

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.example.bidder.R
import com.example.bidder.adapter.ViewPagerAdapter
import com.example.bidder.ui.activity.list_barang.ListBarangActivity
import com.example.bidder.ui.activity.notifikasi.NotifikasiActivity
import com.example.bidder.ui.fragment.aktivitas.BerandaFragmentContract
import com.example.bidder.ui.fragment.aktivitas.BerandaFragmentPresenter
import com.luteh.mangareader.common.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_beranda.*
import java.util.*

class BerandaFragment : BaseFragment(), BerandaFragmentContract.View {

    var presenter = BerandaFragmentPresenter()
    lateinit var viewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater!!.inflate(R.layout.fragment_beranda, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()

        //SLIDER
        viewPager = view?.findViewById(R.id.view_pager)!!
        val adapter = ViewPagerAdapter(this.activity!!)
        viewPager.adapter = adapter
        val timer = Timer()
        timer.scheduleAtFixedRate(MyTimeTask(), 4000, 6000)

        onClickListener()

    }

    //SLIDER
    private inner class MyTimeTask : TimerTask() {
        override fun run() {

            activity?.runOnUiThread {
                when (viewPager.currentItem) {
                    0 -> {
                        viewPager.currentItem = 1
                    }
                    1 -> {
                        viewPager.currentItem = 2
                    }
                    2 -> {
                        viewPager.currentItem = 3
                    }
                    else -> {
                        viewPager.currentItem = 0
                    }
                }
            }
        }
    }

    override fun initView() {
        presenter.postData()
    }


    @SuppressLint("ShowToast")
    override fun onClickListener() {
        btn_lelang_smartphone.setOnClickListener {
            startActivity(Intent(activity, ListBarangActivity::class.java))
        }

        btn_lelang_camera.setOnClickListener {
            startActivity(Intent(activity, ListBarangActivity::class.java))
        }

        btn_lelang_laptop.setOnClickListener {
            startActivity(Intent(activity, ListBarangActivity::class.java))
        }

        btn_lelang_mobil.setOnClickListener {
            startActivity(Intent(activity, ListBarangActivity::class.java))
        }

        btn_lelang_motor.setOnClickListener {
            startActivity(Intent(activity, ListBarangActivity::class.java))
        }

        btn_lelang_sepeda.setOnClickListener {
            startActivity(Intent(activity, ListBarangActivity::class.java))
        }

        btn_notification.setOnClickListener {
            startActivity(Intent(activity, NotifikasiActivity::class.java))
            // Toast.makeText(activity, "Coming Soon Fitur !", Toast.LENGTH_LONG).show()
        }

        btn_top_up.setOnClickListener {
            Toast.makeText(activity, "Coming Soon Fitur !", Toast.LENGTH_LONG).show()
        }

        btn_voucher.setOnClickListener {
            Toast.makeText(activity, "Coming Soon Fitur !", Toast.LENGTH_LONG).show()
        }
    }

    override lateinit var rootView: View

    override fun onInit() {
        initView()
    }
}