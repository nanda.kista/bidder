package com.example.bidder.ui.activity.notifikasi

import com.example.bidder.model.Barang
import com.example.bidder.common.base.BaseContract
import com.example.bidder.model.Notif

class NotifikasiContract {
    interface View : BaseContract.View {

        fun initView()
        fun showRecyclerList(list : ArrayList<Notif>)
        fun progressBarShow()
        fun progressBarHide()
        fun onErrorGetNotif(msg : String)
        fun onSwipeRefresh()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun onLoading(state:Boolean)
        fun getListNotif()
    }
}