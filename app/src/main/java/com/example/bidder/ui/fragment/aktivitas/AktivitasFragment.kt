package com.example.bidder.ui.fragment.aktivitas

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bidder.R
import com.example.bidder.adapter.ListAktivitasAdapter
import com.example.bidder.dummy.AktivitasData
import com.example.bidder.model.Barang
import com.example.bidder.ui.activity.riwayat.RiwayatActivity
import com.luteh.mangareader.common.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_aktivitas.*

class AktivitasFragment : BaseFragment(), AktivitasFragmentContract.View {

    var presenter = AktivitasFragmentPresenter()
    private var listBarang: ArrayList<Barang> = arrayListOf()

    override lateinit var rootView : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater!!.inflate(R.layout.fragment_aktivitas, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()

        showRecyclerList(listBarang)
        listBarang.addAll(AktivitasData.listBarang)

    }


    override fun onInit() {
        initView()
    }

    override fun initView() {
        rv_aktivitas.setHasFixedSize(true)
        //Menu ActionBar
        btn_riwayat.setOnClickListener {
            startActivity(Intent(activity, RiwayatActivity::class.java))
        }
    }

    override fun showRecyclerList(list: ArrayList<Barang>) {
        val listAktivitas = ListAktivitasAdapter(list)
        listAktivitas.notifyDataSetChanged()
        rv_aktivitas.layoutManager = LinearLayoutManager(activity)
        rv_aktivitas.adapter = listAktivitas
        presenter.onLoading(false)
    }

    override fun progressBarShow() {
        pb_aktivitas.visibility = View.VISIBLE
    }

    override fun progressBarHide() {
        pb_aktivitas.visibility = View.GONE
    }

}