package com.example.bidder.ui.activity.detail_barang

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.models.SlideModel
import com.example.bidder.R
import com.example.bidder.common.base.BaseActivity
import kotlinx.android.synthetic.main.activity_detail_barang.*

class DetailAktivitasRiwayatActivity : BaseActivity(), DetailBarangContract.View {

    private var presenter = DetailBarangPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_barang)
        presenter.attach(this)

        //TOOLBAR
        setSupportActionBar(tb_detail_lelang)
        supportActionBar?.title = "Detail Barang"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter.getDescription()

    }

    override fun onInit() {
        initView()
    }

    override fun initView() {
        onSwipeRefresh()
        visibilityDetailAktivitas()
    }

    override fun progressBarHide() {
        pb_detail_lelang.visibility = View.GONE
        pb_photo_detail.visibility= GONE
    }

    override fun progressBarShow() {
        pb_detail_lelang.visibility = View.VISIBLE
        pb_photo_detail.visibility= VISIBLE
    }

    override fun showDetail() {
        statusOption()

        val photo = intent.getStringExtra(EXTRA_PHOTO)
        val bid = intent.getStringExtra(EXTRA_BID)
        val status = intent.getIntExtra(EXTRA_STATUS, 0)
        Log.d("Detail Activity ", status.toString())

        tv_detail_judul_barang.text = intent.getStringExtra(EXTRA_NAMA)
        tv_detail_harga_barang.text = intent.getStringExtra(EXTRA_HARGA)
        tv_detail_waktu.text = intent.getStringExtra(EXTRA_WAKTU)
        tv_detail_tgl.text = intent.getStringExtra(EXTRA_TANGGAL)
        tv_detail_lokasi.text = intent.getStringExtra(EXTRA_LOKASI)
        tv_detail_uang_jaminan.text = intent.getStringExtra(EXTRA_JAMINAN)
        tv_detail_penyelenggara.text = intent.getStringExtra(EXTRA_PENYELENGGARA)
        tv_detail_bid_teratas.text = "Rp $bid"

        val imageList = ArrayList<SlideModel>()
        imageList.add(SlideModel(photo))
        imageList.add(SlideModel("https://m.jpnn.com/timthumb.php?src=https://photo.jpnn.com/arsip/watermark/2019/12/02/xiaomi-redmi-foto-gizchina-79.jpg&w=600&h=320&a=t&zc=0&q=80",
            true))
//        imageList.add(SlideModel("https://2.bp.blogspot.com/-CyLH9NnPoAo/XJUWK2UHiMI/AAAAAAAABUk/D8XMUIGhDbwEhC29dQb-7gfYb16GysaQgCLcBGAs/s1600/tiger.jpg"))
//        imageList.add(SlideModel("https://3.bp.blogspot.com/-uJtCbNrBzEc/XJUWQPOSrfI/AAAAAAAABUs/ZlReSwpfI3Ack60629Rv0N8hSrPFHb3TACLcBGAs/s1600/elephant.jpg", "The population of elephants is decreasing in the world."))
        val imageSlider = findViewById<ImageSlider>(R.id.iv_detail_photo)
        imageSlider.setImageList(imageList)
    }

    override fun onClickListener() {

    }

    override fun onSwipeRefresh() { sr_detail_lelang.setOnClickListener {
        presenter.onLoading(true)
        presenter.getDescription()
        Handler().postDelayed({ sr_detail_lelang.isRefreshing = false }, 500)
    }
    }

    companion object {
        val EXTRA_NAMA = "nama_barang"
        val EXTRA_STATUS = "nama_status"
        val EXTRA_PHOTO = "photo_barang"
        val EXTRA_HARGA = "harga_barang"
        val EXTRA_DESKRIPSI = "deskripsi_barang"
        val EXTRA_TANGGAL = "tgl_barang"
        val EXTRA_WAKTU = "waktu_barang"
        val EXTRA_LOKASI = "lokasi_barang"
        val EXTRA_PENYELENGGARA = "penyelenggara_barang"
        val EXTRA_BID = "bid_teratas_barang"
        val EXTRA_JAMINAN = "uang_jaminan_barang"
    }

    fun visibilityDetailAktivitas() {
        btn_ikuti_lelang.visibility = GONE
        btn_ajukan_bid.visibility = GONE
    }

    fun statusOption() {
        val status = intent.getIntExtra(EXTRA_STATUS, 0)
        when (status) {
            1 -> {
                tv_detail_status.text = "Sedang Berlangsung"
                tv_detail_status.setBackgroundResource(R.drawable.bg_status_biru)
            }
            2 -> {
                tv_detail_status.text = "Menunggu Pembayaran"
                tv_detail_status.setBackgroundResource(R.drawable.bg_status_kuning)
            }
            3 -> {
                tv_detail_status.text = "Selesai"
                tv_detail_status.setBackgroundResource(R.drawable.bg_status_hijau)
            }
            4 -> {
                tv_detail_status.text = "Penalti"
                tv_detail_status.setBackgroundResource(R.drawable.bg_status_merah)
            }
        }
    }
}