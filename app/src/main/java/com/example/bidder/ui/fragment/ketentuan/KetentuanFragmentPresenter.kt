package com.example.bidder.ui.fragment.aktivitas

import io.reactivex.disposables.CompositeDisposable

class KetentuanFragmentPresenter : KetentuanFragmentContract.Presenter {


    private val TAG = "ListPresenter"

    private lateinit var view: KetentuanFragmentContract.View
    private val subscriptions = CompositeDisposable()

    override fun postData() {
        //DO SOMETHING
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: KetentuanFragmentContract.View) {
        this.view = view
    }

}