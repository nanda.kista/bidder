package com.example.bidder.ui.activity.detail_barang

import com.example.bidder.common.base.BaseContract

class DetailBarangContract {
    interface View : BaseContract.View {

        fun initView()
        fun onSwipeRefresh()
        fun progressBarHide()
        fun progressBarShow()
        fun showDetail()
        fun onClickListener()
    }

    //@PreActivity
    interface Presenter : BaseContract.Presenter<View> {
        fun onLoading(state:Boolean)
        fun getDescription()
    }
}