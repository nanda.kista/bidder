package com.example.bidder.ui.activity.dashboard

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View.GONE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.bidder.R
import com.example.bidder.ui.activity.riwayat.RiwayatActivity
import com.example.bidder.ui.fragment.aktivitas.AktivitasFragment
import com.example.bidder.ui.fragment.beranda.BerandaFragment
import com.example.bidder.ui.fragment.ketentuan.KetentuanFragment
import com.example.bidder.ui.fragment.profile.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class DashboardActivity : AppCompatActivity() {

   // lateinit var viewPager : ViewPager
   private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //BOTTOM NAVBAR
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        //BOTTOM NAVBAR ON  CLICK
        if (savedInstanceState == null) {
            navigation.selectedItemId = R.id.navigation_beranda
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        finish()
    }


    //BottomNavigationBar
    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            val fragment: Fragment
            when (item.itemId) {
                R.id.navigation_beranda -> {
                    fragment = BerandaFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(
                            R.id.container_layout,
                            fragment,
                            fragment.javaClass.simpleName
                        )
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_activitas -> {
                    fragment = AktivitasFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(
                            R.id.container_layout,
                            fragment,
                            fragment.javaClass.simpleName
                        )
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_ketentuan -> {
                    fragment = KetentuanFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(
                            R.id.container_layout,
                            fragment,
                            fragment.javaClass.simpleName
                        )
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_profile -> {
                    fragment = ProfileFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(
                            R.id.container_layout,
                            fragment,
                            fragment.javaClass.simpleName
                        )
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    //Double Back Press for Exit
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}
