package com.example.bidder.ui.fragment.aktivitas

import io.reactivex.disposables.CompositeDisposable

class ProfileFragmentPresenter : ProfileFragmentContract.Presenter {


    private val TAG = "ListPresenter"

    private lateinit var view: ProfileFragmentContract.View
    private val subscriptions = CompositeDisposable()

    override fun postData() {
        //DO SOMETHING
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: ProfileFragmentContract.View) {
        this.view = view
    }

}