package com.example.bidder.ui.activity.notifikasi

import io.reactivex.disposables.CompositeDisposable

class NotifikasiPresenter : NotifikasiContract.Presenter {

    private lateinit var view: NotifikasiContract.View
    private val subscriptions = CompositeDisposable()


    override fun onLoading(state: Boolean) {
        if (state) {
            view.progressBarShow()
        } else
            view.progressBarHide()
    }

    override fun getListNotif() {
        onLoading(false)
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: NotifikasiContract.View) {
        this.view = view
    }
}