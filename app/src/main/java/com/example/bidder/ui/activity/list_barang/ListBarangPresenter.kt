package com.example.bidder.ui.activity.list_barang

import io.reactivex.disposables.CompositeDisposable

class ListBarangPresenter : ListBarangContract.Presenter {

    private lateinit var view: ListBarangContract.View
    private val subscriptions = CompositeDisposable()


    override fun onLoading(state: Boolean) {
        if (state) {
            view.progressBarShow()
        } else
            view.progressBarHide()
    }

    override fun getListBarang() {
        //view.showRecyclerList()
       // view.progressBarHide()
        onLoading(false)
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: ListBarangContract.View) {
        this.view = view
    }
}