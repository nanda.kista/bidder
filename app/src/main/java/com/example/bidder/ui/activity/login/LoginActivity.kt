package com.example.bidder.ui.activity.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.bidder.R
import com.example.bidder.common.Fungsiku
import com.example.bidder.common.base.BaseActivity
import com.example.bidder.common.helper.ValidationHelper
import com.example.bidder.sharedpreference.KeySharedPreference
import com.example.bidder.sharedpreference.SharedPreference
import com.example.bidder.sharedpreference.TokenDummy
import com.example.bidder.ui.activity.dashboard.DashboardActivity
import com.example.bidder.ui.activity.register.ChooseRegisActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), LoginContract.View {

    val TAG = "LoginActivity"
    var presenter = LoginPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        presenter.attach(this)

    }

    override fun onInit() {
        initView()
    }

    override fun initView() {
        val errorMessage = "Harap diisi terlebih dahulu!"
        ValidationHelper.isValidMaterialEditText(et_email_layout, et_username, errorMessage)
        ValidationHelper.isValidMaterialEditText(et_password_layout, et_password, errorMessage)

       onClickListener()
    }

    override fun showErrorWrong() {
        Fungsiku.showPopUpDialogWaring(this, "Login gagal !", "Username atau password salah.")
    }

    override fun showErrorEmpty() {
        Fungsiku.showPopUpDialogWaring(this, "Login gagal !", "Username atau password tidak boleh kosong.")
    }


    override fun onSuccess() {
        val sharedPref = SharedPreference(applicationContext)
        sharedPref.save(KeySharedPreference.KEY_TOKEN, TokenDummy.TOKEN)
        Log.d(TAG, "$sharedPref")

        val intent = Intent(this, DashboardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    override fun onClickListener() {
        btn_go_daftar.setOnClickListener {
            startActivity(Intent(this, ChooseRegisActivity::class.java))
        }

        btn_login.setOnClickListener {
            val username = et_username.text.toString()
            val password  = et_password.text.toString()
            showProgressDialog()
            presenter.checkValidation(username, password)
        }

        btn_lupa_password.setOnClickListener {
            Toast.makeText(this, "Comming Soon Fitur !", Toast.LENGTH_LONG).show()
        }
    }

    override fun showProgressDialog() {
        Fungsiku.showProgressDialog(this, "Loading...")
    }

    override fun dismissProgressDialog() {
        Fungsiku.dismissProgressDialog()
    }


}
