package com.example.bidder.ui.fragment.ketentuan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.bidder.R
import com.example.bidder.ui.fragment.aktivitas.KetentuanFragmentContract
import com.example.bidder.ui.fragment.aktivitas.KetentuanFragmentPresenter
import com.example.bidder.ui.fragment.aktivitas.ProfileFragmentContract
import com.example.bidder.ui.fragment.aktivitas.ProfileFragmentPresenter
import com.luteh.mangareader.common.base.BaseFragment

class KetentuanFragment : BaseFragment(), KetentuanFragmentContract.View {

    var presenter = KetentuanFragmentPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater!!.inflate(R.layout.fragment_ketentuan, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()
    }


    override fun showProgress(show: Boolean) {

    }

    override fun initView() {
        presenter.postData()
    }

    override fun onErrorPostData() {

    }

    override lateinit var rootView: View

    override fun onInit() {
        initView()
    }
}