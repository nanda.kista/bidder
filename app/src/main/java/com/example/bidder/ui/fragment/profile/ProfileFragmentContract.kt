package com.example.bidder.ui.fragment.aktivitas

import com.example.bidder.common.base.BaseContract

class ProfileFragmentContract {
    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun initView()
        fun onErrorPostData()
        fun onClickListener()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun postData()
    }
}