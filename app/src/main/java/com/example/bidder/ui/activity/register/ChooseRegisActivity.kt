package com.example.bidder.ui.activity.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.widget.Toolbar
import com.example.bidder.R
import com.example.bidder.ui.activity.login.LoginActivity
import kotlinx.android.synthetic.main.activity_choose_regis.*

class ChooseRegisActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_regis)

        tb_choose_regis.setOnClickListener {
            finish()
        }

        btn_daftar_pengguna.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        btn_daftar_penyelenggara.setOnClickListener {
            Toast.makeText(this, "Coming Soon Fitur", Toast.LENGTH_LONG).show()
        }

        btn_go_login1.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}
