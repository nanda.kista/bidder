package com.example.bidder.ui.activity.riwayat

import io.reactivex.disposables.CompositeDisposable

class RiwayatPresenter : RiwayatContract.Presenter {

    private lateinit var view: RiwayatContract.View
    private val subscriptions = CompositeDisposable()


    override fun onLoading(state: Boolean) {
        if (state) {
            view.progressBarShow()
        } else
            view.progressBarHide()
    }

    override fun getListRiwayat() {
        //view.showRecyclerList()
       // view.progressBarHide()
        onLoading(false)
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: RiwayatContract.View) {
        this.view = view
    }
}