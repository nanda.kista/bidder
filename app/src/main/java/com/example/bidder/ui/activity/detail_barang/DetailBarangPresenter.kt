package com.example.bidder.ui.activity.detail_barang

import io.reactivex.disposables.CompositeDisposable

class DetailBarangPresenter : DetailBarangContract.Presenter {

   private lateinit var view: DetailBarangContract.View
    private val subscriptions = CompositeDisposable()


    override fun onLoading(state: Boolean) {
        if (state) {
            view.progressBarShow()
        } else
            view.progressBarHide()
    }

    override fun getDescription() {
        //view.showRecyclerList()
        view.showDetail()
        onLoading(false)
        //view.progressBarHide()
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: DetailBarangContract.View) {
        this.view = view
    }
}