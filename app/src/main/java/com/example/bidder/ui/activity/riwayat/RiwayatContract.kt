package com.example.bidder.ui.activity.riwayat

import com.example.bidder.model.Barang
import com.example.bidder.common.base.BaseContract

class RiwayatContract {
    interface View : BaseContract.View {

        fun initView()
        fun showRecyclerList(list : ArrayList<Barang>)
        fun progressBarShow()
        fun progressBarHide()
        fun onErrorGetListRiwayat(msg : String)
        fun onSwipeRefresh()
    }

    //@PreActivity
    interface Presenter : BaseContract.Presenter<View> {
        fun onLoading(state:Boolean)
        fun getListRiwayat()
    }
}