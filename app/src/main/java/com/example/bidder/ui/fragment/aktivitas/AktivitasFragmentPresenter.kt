package com.example.bidder.ui.fragment.aktivitas

import io.reactivex.disposables.CompositeDisposable

class AktivitasFragmentPresenter : AktivitasFragmentContract.Presenter {


    private val TAG = "ListPresenter"

    private lateinit var view: AktivitasFragmentContract.View
    private val subscriptions = CompositeDisposable()

    override fun onLoading(state: Boolean) {
        if (state) {
            view.progressBarShow()
        } else
            view.progressBarHide()
    }

    override fun getListBarang() {
       // view.showRecyclerList()
        // view.progressBarHide()
        onLoading(false)
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: AktivitasFragmentContract.View) {
        this.view = view
    }

}