package com.example.bidder.ui.activity.riwayat

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bidder.R
import com.example.bidder.adapter.ListAktivitasAdapter
import com.example.bidder.common.base.BaseActivity
import com.example.bidder.dummy.RiwayatData
import com.example.bidder.model.Barang
import kotlinx.android.synthetic.main.activity_riwayat.*

class RiwayatActivity : BaseActivity(), RiwayatContract.View {

    private var presenter =  RiwayatPresenter()
    private var listBarang: ArrayList<Barang> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_riwayat)
        presenter.attach(this)

        //TOOLBAR
        setSupportActionBar(tb_riwayat)
        supportActionBar?.title = "Riwayat"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        showRecyclerList(listBarang)
        listBarang.addAll(RiwayatData.listBarang)
    }

    override fun onInit() {
        initView()
    }

    override fun initView() {
        rv_riwayat.setHasFixedSize(true)
        onSwipeRefresh()
    }

    override fun showRecyclerList(list: ArrayList<Barang>) {
        val listRiwayat = ListAktivitasAdapter(list)
        rv_riwayat.layoutManager = LinearLayoutManager(this)
        rv_riwayat.adapter = listRiwayat
        presenter.onLoading(false)
    }

    override fun progressBarHide() {
        pb_riwayat.visibility = View.GONE
    }

    override fun progressBarShow() {
        pb_riwayat.visibility = View.VISIBLE
    }

    override fun onErrorGetListRiwayat(msg: String) {
        Log.d("Riwayat Activity : ", msg)
        Toast.makeText(this, "Cek koneksi internet anda", Toast.LENGTH_SHORT).show()
    }

    override fun onSwipeRefresh() {
        sr_riwayat.setOnRefreshListener {
            presenter.onLoading(true)
            presenter.getListRiwayat()
            Handler().postDelayed({ sr_riwayat.isRefreshing = false }, 500)
        }
    }


}
