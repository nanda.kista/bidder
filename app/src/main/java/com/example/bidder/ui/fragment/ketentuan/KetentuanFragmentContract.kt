package com.example.bidder.ui.fragment.aktivitas

import com.example.bidder.common.base.BaseContract

class KetentuanFragmentContract {
    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun initView()
        fun onErrorPostData()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun postData()
    }
}