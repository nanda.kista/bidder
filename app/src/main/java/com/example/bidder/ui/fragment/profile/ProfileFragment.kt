package com.example.bidder.ui.fragment.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.example.bidder.R
import com.example.bidder.ui.activity.about_us.AboutUsActivity
import com.example.bidder.ui.activity.riwayat.RiwayatActivity
import com.example.bidder.ui.activity.notifikasi.NotifikasiActivity
import com.example.bidder.ui.activity.pengaturan.PengaturanActivity
import com.example.bidder.ui.fragment.aktivitas.ProfileFragmentContract
import com.example.bidder.ui.fragment.aktivitas.ProfileFragmentPresenter
import com.luteh.mangareader.common.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : BaseFragment(), ProfileFragmentContract.View {

    var presenter = ProfileFragmentPresenter()
    private var myContext: FragmentActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater!!.inflate(R.layout.fragment_profile, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()
    }


    override fun showProgress(show: Boolean) {

    }

    override fun initView() {
        presenter.postData()
        onClickListener()

        //btn_profile_aktivitas.setOnClickListener {
//            BottomNavigationView.OnNavigationItemSelectedListener {
//                item ->
//                val fragManager: FragmentManager = myContext!!.supportFragmentManager
//                val fragment = AktivitasFragment()
//                fragManager.beginTransaction().replace(R.id.container_layout, fragment).commit()
//                return@OnNavigationItemSelectedListener true
//            }

//            val fragment = AktivitasFragment()
//            fragmentManager?.beginTransaction()
//                ?.replace(
//                    R.id.container_layout,
//                    fragment,
//                    fragment.javaClass.simpleName
//                )
//                ?.commit()
       // }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onAttach(activity: Activity) {
        myContext = activity as FragmentActivity
        super.onAttach(activity)
    }

    override fun onErrorPostData() {

    }

    override fun onClickListener() {
        btn_profile_notifikasi.setOnClickListener {
            startActivity(Intent(activity, NotifikasiActivity::class.java))
        }

        btn_profile_riwayat.setOnClickListener {
            startActivity(Intent(activity, RiwayatActivity::class.java))
        }

        btn_profile_tentang_kami.setOnClickListener {
            startActivity(Intent(activity, AboutUsActivity::class.java))
        }

        btn_pengaturan.setOnClickListener {
            startActivity(Intent(activity, PengaturanActivity::class.java ))
        }

        btn_profile_topup.setOnClickListener {
            Toast.makeText(activity, "Coming Soon Fitur !", Toast.LENGTH_LONG).show()
        }

        btn_profile_voucher.setOnClickListener {
            Toast.makeText(activity, "Coming Soon Fitur !", Toast.LENGTH_LONG).show()
        }

        btn_profile_bantuan_layanan.setOnClickListener {
            Toast.makeText(activity, "Coming Soon Fitur !", Toast.LENGTH_LONG).show()
        }

        btn_profile_rate.setOnClickListener {
            Toast.makeText(activity, "Coming Soon Fitur !", Toast.LENGTH_LONG).show()
        }
    }

    override lateinit var rootView: View

    override fun onInit() {
        initView()
    }
}