package com.example.bidder.dummy

import com.example.bidder.model.Barang
import com.example.bidder.model.Notif

object NotifData {

    private val Deskripsi = arrayOf(
        "Selamat Anda telah memenangkan Lelang (Samsung Galaxy Note 8 4/128gb Segel). Segera datang ke tempat pelelangan untuk melakukan pembayaran dan mengambil barangmu",
        "(iPhone X 64gb iBox) Segera datang ke tempat pelelangan untuk melakukan pembayaran dan mengambil barangmu berupa iPhone X 64gb iBox",
        "Selamat Anda telah memenangkan Lelang (Notebook Asus ROG GL553VD 16Gb 512SSD). Segera datang ke tempat pelelangan untuk melakukan pembayaran dan mengambil barangmu"
    )

    private val Status = arrayOf(
        1,
        2,
        2
    )

    private val TimeRemaining = arrayOf(
        5,
        31,
        53
    )



    val listBarang: ArrayList<Notif>
        get() {
            val list = arrayListOf<Notif>()
            for (position in Deskripsi.indices) {
                val notif = Notif()
                notif.deskripsi = Deskripsi[position]
                notif.status = Status[position]
                notif.timeRemaining = TimeRemaining[position]
                list.add(notif)
            }
            return list
        }


}