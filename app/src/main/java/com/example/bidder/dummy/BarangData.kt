package com.example.bidder.dummy

import com.example.bidder.model.Barang

object BarangData {

    private val Nama = arrayOf(
        "iPhone 11 Pro Max 256gb Space Grey",
        "Samsung Galaxy 11 6/128gb Black",
        "Samsung Galaxy Note 8 4/128gb Segel",
        "Xiaomi Redmi Note 8 4/64gb Baby Pink",
        "Samsung Galaxy Note 8 4/128gb Segel",
        "iPhone X 256gb Space Grey"
    )

    private val Tanggal = arrayOf(
        "10-11-2019",
        "10-11-2202",
        "10-11-2020",
        "10-11-2012",
        "10-11-2012",
        "10-11-20121"
    )

    private val Jam = arrayOf(
        "10.00 - 12.00 WIB",
        "10.00 - 12.00 WIB",
        "10.00 - 12.00 WIB",
        "10.00 - 12.00 WIB",
        "10.00 - 12.00 WIB",
        "10.00 - 12.00 WIB"
    )

    private val Harga = arrayOf(
        "Rp 8.000.000",
        "Rp 6.000.000",
        "Rp 8.000.000",
        "Rp 11.000.000",
        "Rp 8.000.000",
        "Rp 5.500.000"
    )

    private val Jaminan = arrayOf(
        "Rp 2.000.000",
        "Rp 1.000.000",
        "Rp 2.000.000",
        "Rp 3.000.000",
        "Rp 2.000.000",
        "Rp 850.000"
    )


    private val Bid = arrayOf(
        "9500000",
        "7000000",
        "9000000",
        "12000000",
        "8250000",
        "8000000"
    )

    private val Lokasi = arrayOf(
        "Kota Bandar Lampung",
        "Kab. Pringsewu",
        "Kab. Pringsewu",
        "Kota Bandar Lampung",
        "Kota Bandar Lampung",
        "Kota Bandar Lampung"
    )

    private val Penyelenggara = arrayOf(
        "Dani Cell",
        "MV Counter",
        "Dani Cell",
        "KNPL Bandar Lampung",
        "KNPL Bandar Lampung",
        "MV Counter"
    )

    private val Status = arrayOf(
        1,
        1,
        2,
        2,
        2,
        2
    )

    private val Photo = arrayOf(
        "https://asset-a.grid.id/crop/0x0:0x0/700x465/photo/2019/11/18/3621024652.jpg",
        "https://eratekno.com/wp-content/uploads/2020/03/Redmi-3.jpg",
        "https://m.jpnn.com/timthumb.php?src=https://photo.jpnn.com/arsip/watermark/2019/12/02/xiaomi-redmi-foto-gizchina-79.jpg&w=600&h=320&a=t&zc=0&q=80",
        "hgttps://images.anandtech.com/doci/14892/DSC_0013_678x452.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Redmi_K20_Pro_%28Xiaomi_Mi_9T_Pro%29_front.jpg/1200px-Redmi_K20_Pro_%28Xiaomi_Mi_9T_Pro%29_front.jpg",
        "https://www.tbstat.com/wp/uploads/2019/07/Samsung-Galaxy-S10-1200x675.jpg"
    )


    val listBarang: ArrayList<Barang>
        get() {
            val list = arrayListOf<Barang>()
            for (position in Nama.indices) {
                val barang =
                    Barang()
                barang.nama = Nama[position]
                barang.jam = Jam[position]
                barang.tanggal = Tanggal[position]
                barang.harga = Harga[position]
                barang.lokasi = Lokasi[position]
                barang.penyelenggara = Penyelenggara[position]
                barang.bid = Bid[position]
                barang.jaminan = Jaminan[position]
                barang.photo = Photo[position]
                barang.status = Status[position]
                list.add(barang)
            }
            return list
        }


}