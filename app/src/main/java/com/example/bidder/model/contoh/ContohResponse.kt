package com.example.bidder.model.contoh

import com.example.bidder.model.contoh.ContohData
import com.google.gson.annotations.SerializedName

data class ContohResponse(
    @SerializedName("status")
    val status : Boolean,
    @SerializedName("message")
    val message : String,
    @SerializedName("total")
    val total : Int,
    @SerializedName("data")
    val data : List<ContohData>
)