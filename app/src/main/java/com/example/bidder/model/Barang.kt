package com.example.bidder.model

class Barang (
    var nama: String = "",
    var harga: String = "",
    var tanggal: String = "",
    var jam: String = "",
    var lokasi: String = "",
    var photo: String ="",
    var penyelenggara: String="",
    var jaminan: String="",
    var bid: String="",
    var status: Int= 0
)