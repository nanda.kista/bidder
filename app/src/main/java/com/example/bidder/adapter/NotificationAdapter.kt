package com.example.bidder.adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.bidder.R
import com.example.bidder.model.Barang
import com.example.bidder.model.Notif
import com.example.bidder.ui.activity.detail_barang.DetailAktivitasRiwayatActivity
import com.example.bidder.ui.activity.detail_barang.DetailBarangActivity
import com.example.bidder.ui.activity.detail_barang.DetailNotif_DummyActivity
import kotlinx.android.synthetic.main.layout_list_aktivitas.view.*
import kotlinx.android.synthetic.main.layout_list_notification.view.*

class NotificationAdapter(private val listNotif : ArrayList<Notif>) :
    RecyclerView.Adapter<NotificationAdapter.ListViewHolder>() {

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvDeskripsi : TextView = itemView.findViewById(R.id.tv_notif_deskripsi)
        var tvTimeRemaining : TextView = itemView.findViewById(R.id.tv_notif_waktu)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.layout_list_notification, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listNotif.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val dataNotif = listNotif[position]
        holder.tvDeskripsi.text = dataNotif.deskripsi
        val timeRemaining = dataNotif.timeRemaining
        holder.tvTimeRemaining.text = "$timeRemaining menit yang lalu"

        holder.itemView.setOnClickListener {
            val context = it.context
            val intent = Intent(context, DetailNotif_DummyActivity::class.java)
            context.startActivity(intent)
        }

        when (dataNotif.status) {
            1 -> {
                holder.itemView.bg_notif.setBackgroundResource(R.color.colorBlueList)
            }
        }

    }
}