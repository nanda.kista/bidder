package com.example.bidder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.bidder.R
import kotlinx.android.synthetic.main.slide_layout.view.*

class ViewPagerAdapter : PagerAdapter {

    private val context: Context
    private var layoutInflater: LayoutInflater? = null
    private val images = arrayOf(
        R.drawable.img_slider1,
        R.drawable.img_slider2,
        R.drawable.img_slider3,
        R.drawable.img_slider4
    )

    constructor(context: Context) : super() {
        this.context = context
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater!!.inflate(R.layout.slide_layout, null)
        view.image_view.setImageResource(images[position])
        val vp = container as ViewPager
        vp.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}