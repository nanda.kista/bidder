# Bidder
Aplikasi yang akan kami buat bernama Bidder. Aplikasi ini merupakan platform untuk pelelangan. 
Melalui aplikasi ini orang dapat melakukan lelang tanpa harus datang ke tempatnya. 
Aplikasi ini terdiri dari 2 pengguna yaitu pelelang dan penawar.

# Mockup Application
Berikut adalah lampiran file mockup (Adobe XD) :
[Mockup_Bidder.xd](/uploads/1854956cfc2fed6d5517186f3b6b6e30/Mockup_Bidder.xd)

Berikut adalah gambar mockup aplikasi

<img src="/uploads/c0c2c1c4fcb813171a791153e8f11afe/Login.png" width="300" height="600"> 

<img src="/uploads/502b1da725d9989eb87216bd057d8509/Chose_User.png" width="300" height="600">

<img src="/uploads/32af44a65028b551213025d94463a1d5/Registrasi.png" width="300" height="600">

<img src="/uploads/ed63b228c29c251ffc10f59074031070/Dashboard.png" width="300" height="600">

<img src="/uploads/e69ba5c7309fe89034957de6946cacb0/Notifikasi.png" width="300" height="600">


<img src="/uploads/8c7ec7c90098522755fffe1ba2d58e5f/Daftar_lelang.png" width="300" height="600">

<img src="/uploads/3983c699100fca608647e6a0b0e4bcf6/Detail_barang.png" width="300" height="600">

<img src="/uploads/f13771a080eea3e2735ee29dca328b54/Bid.png" width="300" height="600">

<img src="/uploads/224ca0553545853d62bcb2502b9b0de4/Request_Bid.png" width="300" height="600">



<img src="/uploads/c439371f844dffc0ee4b79631d61b76b/Profile.png" width="300" height="600">

<img src="/uploads/071d7c51f4ff1b1e37b9eac1dd47ff4b/Setting.png" width="300" height="600">

<img src="/uploads/084a4195e257e2a6a7ac5f3245944c7a/Change_Password.png" width="300" height="600">

<img src="/uploads/7a376fa9c803d409490c110b69cea939/Biodata.png" width="300" height="600">

<img src="/uploads/b94ac66dfaa3a428ed4b178c07358ee4/Otp.png" width="300" height="600">



<img src="/uploads/11682141da9e6ca67de2361307f20d30/List_activities.png" width="300" height="600">

<img src="/uploads/7179f9cc99c7ede059d6a850b6b776f5/Empty_activites.png" width="300" height="600">

<img src="/uploads/ea882513573bbec6905c4d635cbc8ee7/Detail_activities.png" width="300" height="600">

<img src="/uploads/09b30a387eb925121dfe12648b9b7b83/Empy_history.png" width="300" height="600">

<img src="/uploads/728625a77f336cb983c88731c5c3db71/History.png" width="300" height="600">
